package br.com.vanilson.model;	

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="obra")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Obra {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long codigo;
	
	@Column
	@NotNull
	private String nome;

	@Column
	private String descricao;

	@Column
	private Date dtpublicacao;
	
	@Column
	private Date dtexposicao;
	
	@Column
	private String img;
	
	@ManyToOne
	@JoinColumn(name="cod_autor")
	private Autor autor;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDtpublicacao() {
		return dtpublicacao;
	}

	public void setDtpublicacao(Date dtpublicacao) {
		this.dtpublicacao = dtpublicacao;
	}

	public Date getDtexposicao() {
		return dtexposicao;
	}

	public void setDtexposicao(Date dtexposicao) {
		this.dtexposicao = dtexposicao;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public long getCodigo() {
		return codigo;
	}
}