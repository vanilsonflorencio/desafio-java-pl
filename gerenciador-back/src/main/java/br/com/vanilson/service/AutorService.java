package br.com.vanilson.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.vanilson.model.Autor;
import br.com.vanilson.model.Obra;
import br.com.vanilson.repository.AutorRepository;
import br.com.vanilson.repository.ObraRepository;


@Scope("application")
@Service
public class AutorService {

	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private ObraRepository obraObraRepository;
	
	public Autor save(Autor autor) {
		return autorRepository.save(autor);
	}	
	public List<Autor> getAll() {
		return autorRepository.findAll();
	}
	public Autor getByCode(long cod) {
		return autorRepository.getOne(cod);
	}
	public void delete(Long cod) {
		autorRepository.deleteById(cod);
	}
	public void delete(Autor autor) {
		autorRepository.delete(autor);
	}
	public Autor adicionarObra(long codautor, long codobra) {
		Autor a = getByCode(codautor);
		Obra  o = obraObraRepository.getOne(codobra);	
		
		//
		return save(a);		
	}
	public Autor update(long cod, Autor autor) {
		Autor original = getByCode(cod);	
		
		BeanUtils.copyProperties(autor, original, "codigo");
		return save(original);		
	}
	public boolean verify() {
		
		return true;
	}
}