package br.com.vanilson.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.vanilson.model.Obra;
import br.com.vanilson.repository.ObraRepository;


@Scope("application")
@Service
public class ObraService {

	@Autowired
	private ObraRepository obraRepository;
	
	public Obra save(Obra obra) {
		return obraRepository.save(obra);
	}	
	public List<Obra> getAll() {
		return obraRepository.findAll();
	}
	public List<Obra> getByName(String titulo) {
		return obraRepository.buscarPorTitulo(titulo.toLowerCase());
	}
	public List<Obra> getByDescription(String descricao) {
		return obraRepository.buscarPorDescricao(descricao.toLowerCase());
	}
	public Obra getByCode(long cod) {
		return obraRepository.getOne(cod);
	}
	public void delete(Long cod) {
		obraRepository.deleteById(cod);
	}
	public void delete(Obra obra) {
		obraRepository.delete(obra);
	}
	public Obra update(long cod, Obra obra) {
		Obra original = getByCode(cod);	
		
		BeanUtils.copyProperties(obra, original, "codigo");
		return save(original);		
	}
	
	public boolean verify() {		
		return true;
	}
}