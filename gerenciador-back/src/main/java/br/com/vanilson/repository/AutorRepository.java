package br.com.vanilson.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vanilson.model.Autor;



public interface AutorRepository  extends JpaRepository<Autor, Long>{
	

}
