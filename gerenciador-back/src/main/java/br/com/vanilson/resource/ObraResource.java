package br.com.vanilson.resource;


import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import br.com.vanilson.model.Obra;
import br.com.vanilson.service.ObraService;



@RestController
@RequestMapping("/obra")
@CrossOrigin(maxAge=10, origins="*")
//@MultipartConfig(maxFileSize = 1024*5, maxRequestSize = 1024*5)

public class ObraResource {	
	
	@Autowired
	private ObraService obraService;
	
	/**
	 * Used to get all registers of the Obra tables.
	 * @return List Obra
	 */
	@GetMapping ("/all")
	public  List<Obra> listar(){
		return obraService.getAll();
	}
	
	/**
	 * <strong>Method used to add a new Obra entity on system.</strong>
	 * 
	 * @param Obra entity
	 * @param HttpServletResponse response
	 * @return ResponseEntity
	 */
	@PostMapping
	public ResponseEntity<Obra> criar(@Valid @RequestBody Obra obra, HttpServletResponse response) {
		Obra obraSalvo = obraService.save(obra);		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(obraSalvo);
	}
	
	
	

	@GetMapping("/{codigo}")
	public ResponseEntity<Obra> buscarPeloCodigo(@PathVariable Long codigo) {
		Obra obra = obraService.getByCode(codigo);
		
		if (obra != null)
			return ResponseEntity.ok(obra);
		else
			return ResponseEntity.notFound().build();
	}
	@GetMapping("/titulo/{titulo}")
	public ResponseEntity <List<Obra>> buscarPeloTitulo(@PathVariable String titulo) {
		List <Obra> obra = obraService.getByName(titulo);
		
		if (obra != null)
			return ResponseEntity.ok(obra);
		else
			return ResponseEntity.notFound().build();
	}
	
	@GetMapping("/descricao/{descricao}")
	public ResponseEntity <List<Obra>> buscarPelaDescricao(@PathVariable String descricao) {
		List <Obra> obra = obraService.getByDescription(descricao);
		
		if (obra != null)
			return ResponseEntity.ok(obra);
		else
			return ResponseEntity.notFound().build();
	}
	
	/**
	 * Used to developers to know all atributes used on this class.
	 * @return Obra A single instance with all values default.
	 */
	@GetMapping("/help")
	public Obra help() {
		Obra obra = new Obra(); 		
		return obra;
	}
	
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long codigo) {
		obraService.delete(codigo);
	}

	@PutMapping("/{codigo}")
	public  ResponseEntity<Obra> atualizarUsuario(@PathVariable Long codigo,  @RequestBody Obra obra) {
		Obra newobra = (Obra) obraService.update(codigo, obra);
		return ResponseEntity.ok(newobra);
	}
}
