
import { ObraDTO                 } from '../../model/obra.dto';
import { TransferService         } from 'src/service/transferService';
import { GeneralServiceComponent } from 'src/service/generalService';
import { AlertController         } from '@ionic/angular';
import { Component               } from '@angular/core';


@Component({
  selector: 'app-obra',
  templateUrl: './obra.page.html',
  styleUrls: ['./obra.page.scss'],
})
export class ObraPage {

  constructor(
    public transfer : TransferService ,
    public genSer   : GeneralServiceComponent,
    public alertCtrl: AlertController
    ) {

  }

  protected showCreate : boolean;
  protected showSearch : boolean;
  protected showExclude: boolean;
  protected showAll    : boolean;

  protected edit: boolean;

  public nome         : string;
  public descricao    : string;
  public dtPublicacao : string;
  public dtexposicao  : string;
  public img          : string;
  private codigo : number

  public obras: ObraDTO[];
  public obra : ObraDTO;


 

  public alterCreate() {
    this.hideAll();
    this.clear();

    this.showCreate = !this.showCreate;
  }
  public alterSearch() {
    this.hideAll();
    this.clear();

    this.showSearch = !this.showSearch;
  }
  public alterGetAll() {
    this.hideAll();
    this.showAll = !this.showAll;

    this.transfer.findAllObra().subscribe((result: any) => {
      this.obras = result;
    });
  }
  public findByName() {
    this.descricao = "";
    this.transfer.findByTitle( this.nome).subscribe((result: any) => {
      this.obras = result;
    },
    (error) => {
      this.obras = null;
      }  
    ); 
  }
  public findByDescription() {
    this.nome = "";
    this.transfer.findByDescription( this.descricao).subscribe((result: any) => {
      this.obras = result;
    },
    (error) => {
      this.obras = null;
      }  
    ); 
  }
  public save() {

    var data = <ObraDTO> 
      {
        nome         : this.nome,
        descricao    : this.descricao,
        dtpublicacao : this.dtPublicacao,
        dtexposicao  : this.dtexposicao,
        img          : this.img,
      };


    this.transfer.saveObra(data).subscribe((result: any) => { 
      this.genSer.showMessage('Salvo com sucesso!');

      this.clear();
    },
    (error) => {
      this.genSer.showMessage('Erro: ' + error.error.error);
      }  
    ); 
  }

  public saveChange() {

    var data = <ObraDTO> 
      {
        nome         : this.nome,
        descricao    : this.descricao,
        dtpublicacao : this.dtPublicacao,
        dtexposicao  : this.dtexposicao,
        img          : this.img,
        codigo       : this.codigo
      };


    this.transfer.editObra(data).subscribe((result: any) => { 
      this.genSer.showMessage('Salvo com sucesso!');

      this.clear();
    },
    (error) => {
      this.genSer.showMessage('Erro: ' + error.error.error);
      }  
    ); 
  }
  public editObra(obra: ObraDTO){
    this.alterCreate();
    this.edit = true;

    this.nome          = obra.nome;
    this.descricao     = obra.descricao;
    this.dtexposicao   = obra.dtexposicao;
    this.dtPublicacao  = obra.dtpublicacao;
    this.codigo        = obra.codigo;
    this.img           = obra.img;
  
  }
    
  public async del(obra: ObraDTO) {
    const alert = await this.alertCtrl.create({
      header: 'Atenção',
      message: 'Tem certeza que deseja excluir a obra ' + obra.nome ,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

          }
        }, {
          text: 'Excluir',
          handler: () => {
            this.transfer.delObra(obra.codigo).subscribe((result: any) => {
              this.genSer.showMessage('Excuido com sucesso!');
              this.alterGetAll();
            },
            (error) => {
              this.genSer.showMessage('Erro ao excluir.');
            }
          );}
        }
      ]
    });

    await alert.present();
    
    
  }
  private hideAll() {
    // this.genSer.presentAlert();
    this.showCreate  = false;
    this.showExclude = false;
    this.showSearch  = false;
    this.showAll     = false;

  }

  public clear() {
    this.nome          = null;
    this.descricao     = null;
    this.dtexposicao   = null;
    this.dtPublicacao  = null;
    this.img           = null;
    this.codigo        = 0;

    this.edit = false;
  }
}
