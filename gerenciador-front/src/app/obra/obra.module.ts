import { NgModule             } from '@angular/core';
import { CommonModule         } from '@angular/common';
import { FormsModule          } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TransferService      } from 'src/service/transferService';

import { IonicModule } from '@ionic/angular';
import { ObraPage    } from './obra.page';

const routes: Routes = [
  {
    path: '',
    component: ObraPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    TransferService,
  ],
  declarations: [ObraPage]
})
export class ObraPageModule {}
