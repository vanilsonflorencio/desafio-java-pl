import { Component, ErrorHandler               } from '@angular/core';
import { AutorDTO                } from '../../model/autor.dto';
import { TransferService         } from 'src/service/transferService';
import { GeneralServiceComponent } from 'src/service/generalService';
import { AlertController         } from '@ionic/angular';
import { ObraDTO } from 'src/model/obra.dto';

@Component({
  selector: 'app-autor',
  templateUrl: 'autor.page.html',
  styleUrls: ['autor.page.scss']
})
export class AutorPage {

  constructor(
    public transfer : TransferService ,
    public genSer   : GeneralServiceComponent,
    public alertCtrl: AlertController
    ) {

  }

  protected showCreate : boolean;
  protected showSearch : boolean;
  protected showExclude: boolean;
  protected showAll    : boolean;
  protected showCpf    : boolean = true;

  protected edit: boolean;

  public nome         : string;
  public pais         : string;
  public cpf          : string;
  public dtnascimento : string;
  public sexo         : string;
  public email        : string;

  private cod         : number;

  public autores: AutorDTO[];
  public autor  : AutorDTO;  


  public checkCountry() {
    
    this.showCpf = !(this.pais.toLowerCase() === 'brasil');
  }

  public alterCreate() {
    this.hideAll();
    this.clear();
    
    this.showCreate = !this.showCreate;
  }
  public alterSearch() {
    this.hideAll();
    this.showSearch = !this.showSearch;
  }
  public alterGetAll() {
    this.hideAll();
    this.showAll = !this.showAll;

    this.transfer.findAllAutor().subscribe((result: any) => {
      this.autores = result;
    });
  }

  public save() {

    var data = <AutorDTO> 
      {
        nome: this.nome,
        cpf  : this.cpf,
        dtnascimento : this.dtnascimento,
        pais : this.pais,
        sexo : this.sexo,
        email: this.email
      };


    this.transfer.saveAutor(data).subscribe((result: any) => { 
      this.genSer.showMessage('Salvo com sucesso!');

      this.clear();
    },
    (error) => {
      this.genSer.showMessage('Erro' + error.error.error);
      }  
    ); 
  }
  public async link(autor: AutorDTO){
      const alert = await this.alertCtrl.create({
        header: 'Defina a obra que pertence ao usuario ' + autor.nome,
        inputs: [
          {
            value: 'code',
            name: 'codigo',
            type: 'number',
            placeholder: ''
          }
        ],        
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          },{
            text: 'Salvar',
            handler: data => {
              // autor.obra = {this.transfer.getObra(data.codigo);
              // this.editUser(autor);
              // console.log(data.codigo);
              this.genSer.showMessage('Ainda não implementado.  :´(');
            }
          }
        ]
    });
  
    await alert.present();
    
  }
  public saveChange() {

    var data = <AutorDTO> 
      {
        nome         : this.nome,
        cpf          : this.cpf,
        dtnascimento : this.dtnascimento,
        pais         : this.pais,
        sexo         : this.sexo,
        email        : this.email,
        codigo       : this.cod
      };


    this.transfer.editAutor(data).subscribe((result: any) => {
      this.genSer.showMessage('Salvo com sucesso!');

      this.clear();
    },
    (error) => {
      this.genSer.showMessage('Erro ' + error.error.error);
      }  
    ); 
  }
  public editUser(autor: AutorDTO){
    this.alterCreate();
    this.edit = true;

    this.nome          = autor.nome;
    this.pais          = autor.pais;
    this.cpf           = autor.cpf;
    this.dtnascimento  = autor.dtnascimento;
    this.sexo          = autor.sexo;
    this.email         = autor.email;
    this.cod           = autor.codigo;
  }
    
  public async del(autor: AutorDTO) {
    const alert = await this.alertCtrl.create({
      header: 'Atenção',
      message: 'Tem certeza que deseja excluir o autor ' + autor.nome ,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

          }
        }, {
          text: 'Excluir',
          handler: () => {
            this.transfer.delAutor(autor.codigo).subscribe((result: any) => {
              this.genSer.showMessage('Excuido com sucesso!');
              this.alterGetAll();
            },
            (error) => {
              this.genSer.showMessage('Erro ao excluir.');
            }
          );}
        }
      ]
    });

    await alert.present();
    
    
  }
  private hideAll() {
    // this.genSer.presentAlert();
    this.showCreate  = false;
    this.showExclude = false;
    this.showSearch  = false;
    this.showAll     = false;

  }

  public clear() {
    this.nome          = null;
    this.pais          = null;
    this.cpf           = null;
    this.dtnascimento  = null;
    this.sexo          = '';
    this.email         = '';
    this.cod           = 0;

    this.edit = false;
  }
}
