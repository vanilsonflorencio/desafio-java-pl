import { IonicModule  } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule     } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule  } from '@angular/forms';
import { AutorPage    } from './autor.page';
import { TransferService } from 'src/service/transferService';
// import { GeneralService } from 'src/service/generalService';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: AutorPage }])
  ],
  providers: [
    TransferService,
    // GeneralService
  ],
  declarations: [AutorPage]
})

export class AutorPageModule {

}

