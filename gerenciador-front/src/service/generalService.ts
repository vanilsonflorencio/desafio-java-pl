import { AlertController } from '@ionic/angular';
import { Injectable      } from '@angular/core';



@Injectable()
export class GeneralServiceComponent {

  constructor(public alertCtrl: AlertController) {}

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }

 
  public async showMessage(msg: string){  
    const alert = await this.alertCtrl.create({
      header: msg,
      subHeader: '',
      // message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }
}
