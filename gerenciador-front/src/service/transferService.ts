
import { HttpClient, HttpHeaders} from '@angular/common/http';

import { Injectable      } from '@angular/core';
import { Observable      } from 'rxjs';
import { AutorDTO        } from 'src/model/autor.dto';
import { ObraDTO         } from 'src/model/obra.dto';


@Injectable()
export class TransferService {

    urlBase = 'http://localhost:8080/';

    constructor(
        public http: HttpClient
       ) {
    }



    public findAllAutor(): Observable<AutorDTO[]> {
        return this.http.get<AutorDTO[]>(this.urlBase + 'autor/all');
    }

    public delAutor(cod: number): Observable<AutorDTO[]> {
        return this.http.delete<AutorDTO[]>(this.urlBase + `autor/${cod}`);
    }
    public saveAutor(data: AutorDTO) {
        const httpOptions = {
            headers: new HttpHeaders({
                'contentType': 'application/json',
                'dataType'   : 'json',
                'crossDomain': 'true',
            })
        };       
        return  this.http.post (this.urlBase + `autor`, data, httpOptions);
    }
    public editAutor(data: AutorDTO) {
        const httpOptions = {
            headers: new HttpHeaders({
                'contentType': 'application/json',
                'dataType'   : 'json',
                'crossDomain': 'true',
            })
        };       
        return  this.http.put (this.urlBase + `autor/${data.codigo}`, data, httpOptions);
    }


    public findAllObra(): Observable<ObraDTO[]> {
        return this.http.get<ObraDTO[]>(this.urlBase + 'obra/all');
    }
    public findByTitle(titulo : string): Observable<ObraDTO[]> {
        return this.http.get<ObraDTO[]>(this.urlBase + `obra/titulo/${titulo}`);
    }
    public getObra(cod : number): Observable<ObraDTO> {
        return this.http.get<ObraDTO>(this.urlBase + `obra/${cod}`);
    }
    public findByDescription(desc : string): Observable<ObraDTO[]> {
        return this.http.get<ObraDTO[]>(this.urlBase + `obra/descricao/${desc}`);
    }
    public delObra(cod: number): Observable<ObraDTO[]> {
        return this.http.delete<ObraDTO[]>(this.urlBase + `obra/${cod}`);
    }
    public saveObra(data: ObraDTO) {
        const httpOptions = {
            headers: new HttpHeaders({
                'contentType': 'application/json',
                'dataType'   : 'json',
                'crossDomain': 'true',
            })
        };       
        return this.http.post (this.urlBase + `obra`, data, httpOptions);
    }
    public editObra(data: ObraDTO) {
        const httpOptions = {
            headers: new HttpHeaders({
                'contentType': 'application/json',
                'dataType'   : 'json',
                'crossDomain': 'true',
            })
        };       
        return  this.http.put (this.urlBase + `obra/${data.codigo}`, data, httpOptions);
    }
}
