import { AutorDTO } from './autor.dto';

export interface ObraDTO{
    codigo        : number,
    nome          : string, 
    descricao     : string,
    dtpublicacao  : string,
    dtexposicao   : string,
    img           : string,
    autor          : AutorDTO[]    
}