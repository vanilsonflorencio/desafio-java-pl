import { ObraDTO } from './obra.dto';


export interface AutorDTO{
    codigo        : number,
    nome          : string, 
    sexo          : string,
    email         : string,
    pais          : string,
    cpf           : string,
    dtnascimento  : string,
    obra          : ObraDTO[];
    
  
    
}